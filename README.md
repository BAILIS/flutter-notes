# Flutter笔记

## 问题解决方案

#### 跨域问题

```
flutter run -d chrome --web-renderer html （或canvaskit）//运行命令
flutter build web --web-renderer html （或canvaskit） //编译打包
```
## 开发笔记

#### MaterialApp

```
MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.yellow//修改主题色
      ),
    )
```
#### Container

```
Container(
        height: 300,//宽度 铺满 double.infinity
        width: 300,//高度  铺满 double.infinity
        decoration: BoxDecoration(
            color: Colors.yellow,//设置控件颜色
             borderRadius:BorderRadius.all(
              Radius.circular(20),//设置边框整体圆角大小
            ),//或者borderRadius: BorderRadius.circular(30),
            border: Border.all(color: Colors.blue, width: 2.0),
             image: DecorationImage(//容器加载图片
              image:NetworkImage(url),
                fit: BoxFit.cover
            ),
            ),//设置控件边框颜色及大小
            margin: EdgeInsets.fromLTRB(left, top, right, bottom),//上线左右边距(margin: EdgeInsets.all(80)//全局外边距80)
            transform: Matrix4.translationValues(x, y, z),// x 左右移动 y 上下移动 100,0,0
            (Matrix4.rotationX(1.8)垂直翻转，Matrix4.skew(1, 3)倾斜，Matrix4.diagonal3Values(1, 1, 1)动画宽高拉伸，默认为1)
            alignment: Alignment.center,//设置内部控件位置
            padding: EdgeInsets.all(20),//全局内边距20(EdgeInsets.fromLTRB(left, top, right, bottom)//上线左右边距)
      )
```


#### Text

```
Text(
        "你好的",
        textDirection: TextDirection.ltr,//字体从左往右
        textAlign: TextAlign.left,//文本居左
        overflow: TextOverflow.ellipsis,//文本超出部分以...显示
        maxLines: 1,//文本只显示一行
        textScaleFactor: 2,//字体放大两倍
        style: TextStyle(
          fontSize: 25.0,//字体大小
          color: Colors.yellow, //也可以 color: Color.fromARGB(255, 255, 0, 1)
          fontWeight: FontWeight.w800,//字体粗细程度(FontWeight.bold为加粗,FontWeight.normal为正常体)
          fontStyle: FontStyle.italic,//斜体
          decoration: TextDecoration.lineThrough//文本废弃线(直线)
          decorationColor: Colors.white//废弃线颜色
          decorationStyle: TextDecorationStyle.dashed//废弃线变为虚线
          letterSpacing: 5.0//文本字间距
        ),
      )
```
#### Image
##### Image.network//网络图片

```
Image.network(url,
              width: 200,
              height: 200,
              alignment: Alignment.bottomRight,//图片对齐区域
              color: Colors.green,//图片颜色
              colorBlendMode: BlendMode.screen,//混合模式
              fit: BoxFit.fill,//充满变形 cover充满不变形,多出部分裁剪掉  contain自适应充满不变形 fitHeight纵向充满不拉伸多出部分裁剪 fitWidth横向充满不拉伸多出部分裁剪
               repeat: ImageRepeat.repeat,//复制平铺repeatx X轴平铺 repeaty Y轴平铺
            )

```
##### repeat多图片平铺效果 <br>
![输入图片说明](imageImageNewWorkRepeat.png)

#### ClipOval椭圆组件

```
ClipOval(//画圆
        child: Image.network(
          url,
          width: 200,
          height: 200,
          fit:BoxFit.cover, 
        ),
      )
```
#### ListView列表组件

```
ListView(
      padding: EdgeInsets.all(10),
      scrollDirection: Axis.horizontal,//横向布局 vertical纵向布局 
      children: <Widget>[
        ListTile(
          leading: Image.network(url),//左图片
          trailing: Icon(Icons.access_alarm),//右图片
          title: Text('一级标题一级标题一级标题一级标题',
          style: TextStyle(
            fontSize: 24,
          )),
          subtitle: Text('二级标题二级标题二级标题二级标题二级标题二级标题'),
        ),

       Image.network("https://51yangyu.cn/icon.jpg"),
        Container(
          child: Text('我是一个标题',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 28
          ),),
          height: 40,
          padding: EdgeInsets.all(10),
        )//ListView可包含任意组件
      ],
    )
```

```
//动态列表
class HomeContent extends StatelessWidget {

  List<Widget> _getData() {
    List<Widget> list = [];
    for (var i = 0; i < 20; i++) {
      list.add(ListTile(
        title: Text('我是$i列表'),
      ));
    }
    return list;
  }//自定义方法实现

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: this._getData(),
    );
  }
}
```

```
class HomeContent extends StatelessWidget {

  List<Widget> _getData() {
    var tempList=listData.map((value){
      return ListTile(
      leading: Image.network(value"xxxx"),//"xxxx"为数组属性名
    title: Text(value["xxxx"]),
    subtitle: Text(value["xxxx"]),
      );
    })
    return tempList.toList();
  }//自定义方法实现，listData是另一个包，放的数组

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: this._getData(),
    );
  }
}


```

```
class HomeContent extends StatelessWidget {
  List list = [];

  HomeContent() {
    for (var i = 0; i < 20; i++) {
      this.list.add("我是第$i条");
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: this.list.length,//指定长度
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(this.list[index]),
          );
        }//循环
     );
  }
}
//利用ListView.builder实现
```

```
class HomeContent extends StatelessWidget {

  Widget _getListData(context, index) {
    return ListTile(
      title: Text(listData[index]["xxxx"]),//"xxxx"为数组属性名
      leading: Image.network(listData[index]["xxxx"]),
      subtitle: Text(listData[index]["xxxx"]),
    );}


    @override
    Widget build(BuildContext context) {
      return ListView.builder(
          itemCount: listData.length,
          itemBuilder: this._getListData);
    }
  }/自定义方法并利用ListView.builder实现，listData是另一个包，放的数组
```
#### GridView组件
    
```
Widget build(BuildContext context) {
      return GridView.count(
        crossAxisSpacing: 20.0,//水平Widget之间的距离
        mainAxisSpacing: 20.0,//垂直Widget之间的距离
        padding: EdgeInsets.all(10),//边缘距离
        childAspectRatio: 0.7,//宽度和高度的比例
        crossAxisCount: 3,//一行的Widget的数量
        children: _getListData(),
      );
    }//GridView.count静态网格布局
```
![输入图片说明](QQ%E5%9B%BE%E7%89%8720220726185531.png)
![输入图片说明](image.png)
//GridView.builder动态网格布局（写法类似于ListView.builder），以上截图是添加属性需要加入的代码
<br>
#### padding组件

```
可利用child子组件在外层套padding
Edgelnsetss设置填充的值 
```

#### Row水平布局组件、Colum垂直布局组件
![输入图片说明](aaa.png)

#### Expanded组件
![输入图片说明](a1.png)
![输入图片说明](a2.png)
![输入图片说明](a.png)

#### Stack组件

```
 Container(
        height: 400,
        width: 300,
        color: Colors.red,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Icon(
                Icons.home,
                size: 40,
                color: Colors.white,
              ),
            ),)//Stack结合Align调整位置
```

```
Container(
        height: 400,
        width: 300,
        color: Colors.red,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 10,
              child: Icon(
                Icons.home,
                size: 40,
                color: Colors.white,
              ),
            ),)//Stack结合Positioned调整位置
```
#### AspectRatio组件

```
Container(
      width: 300,
      child: AspectRatio(
        aspectRatio: 2 / 1,
        child: Container(
          color: Colors.red,
        ),
      ),
    )//通过AspectRatio设置子元素宽高比
```
#### Card组件

```
ListView(
      children: <Widget>[
        Card(
          margin: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text('张三',style: TextStyle(fontSize: 28),),
                subtitle: Text('高级工程师'),
              ),
            ],
          ),
        ),
      ],
    )
```
#### Wrap组件
![输入图片说明](a3.png)

```
Container(
      height: 600,
      width: 300,
      color: Colors.pink,
      padding: EdgeInsets.all(10),
      child: Wrap(
        spacing: 10,
        runSpacing:10,
        // alignment: WrapAlignment.end,
        // runAlignment: WrapAlignment.center,
        direction: Axis.vertical,
        children: <Widget>[
          MyButton('第1季'),
          MyButton('第2季'),
          MyButton('第3季'),
          MyButton('第4季'),
          MyButton('第5季'),
          MyButton('第6季'),
          MyButton('第7季'),
          MyButton('第8季'),
        ],
      ),
    )
```
#### bottomNavigationBar组件

```
import 'package:flutter/material.dart';
import 'Tabs/Home.dart';
import 'Tabs/Setting.dart';
import 'Tabs/Category.dart';

class Tabs extends StatefulWidget {
  const Tabs({Key? key}) : super(key: key);

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {

  int _currentIndex=0;

  List _pageList=[
    HomePage(),
    CategoryPage(),
    SettingPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('洋芋博客'),
      ),
      body: this._pageList[this._currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this._currentIndex,//配置对应的索引值选中
        onTap: (int index){
          setState(() {//改变状态
            this._currentIndex=index;
          });
        },
        iconSize: 40,//icon的大小
        fixedColor: Colors.red,//选中的颜色
        type: BottomNavigationBarType.fixed,//配置底部可以有多个按钮
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: '主页'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: '分类'
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: '设置'
          ),
        ],
      ),
    );
  }
}

```
#### 基本路由跳转
  
```
      RawMaterialButton(
          child: Text('跳转到搜索页面'),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => SearchPage())
            );
          },
        )
```

```
    floatingActionButton:FloatingActionButton(
        child: Text('返回'),
        onPressed: () {
          Navigator.of(context).pop();//跳转上一页
        },
      )
```
#### 命名路由

```
MaterialApp(
      home: Tabs(),
      routes: {
        '/search':(context)=>SearchPage(),//配置路由
      },
    )
```
  
```
      RawMaterialButton(
          child: Text('跳转到搜索页面'),
          onPressed: () {
            Navigator.pushNamed(context, '/search');//路由使用
          },
        )
```

